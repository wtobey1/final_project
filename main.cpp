/* Will Tobey, Nick Lemanski
 * 5/2/17
 * 600.120
 */

#include <string>
#include <fstream>
#include <iostream>
#include <stdio.h>

#include "Terminal.h"
#include "Chess.h"
#include "Prompts.h"


int main(){

    //Start the prompt menu
    Prompts p;
    p.menu();

    //Get initial input MUST ONLY BE 1 or 2
    std::string x;

    //std::cin.ignore();
    //std::getline (std::cin, x);
    std::cin >> x;
    int input = x.at(0) - '0';

    // Moved game initialization outside
    ChessGame game;

    //Checks until we get the proper input from user
    while((input != 1 && input != 2) || x.size() > 1 ){
        p.menu();
        std::cin >> x;
        input = x.at(0) - '0';
    }

    //If new game is chosen
    if(input == 1){

        game.setupBoard();
        game.run();
        
    }

    //If saved game is chosen
    if(input == 2){

        //Ask user for name of saved game
        //Stores it in string gameLoading
        p.loadGame();
        std::string gameLoading;
        std::cin >> gameLoading;  

        std::ifstream file;
        file.open( gameLoading );

        //game.setupBoard(file);
        //Checks if loading the file failed
        bool y = file.good();

        //Runs the failed method and quits
        if(y == 0){
            p.loadFailure();
            return 1;
        }else{
            
            //Sets ups the saved board
            game.setupBoard(file);
        
        }
    

        //RUN the Game
        game.run();
    
    }

    return 0;    
}
