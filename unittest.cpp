
#include "Chess.h"

// Subclass
class TestChessGame : public ChessGame {
    
public:
    
    TestChessGame() : ChessGame() {};
    
    void setupPawnTest();
    void setupRookTest();
    void setupKnightTest();
    void setupBishopTest();
    void setupQueenTest();
    void setupKingTest();
    
};

void TestChessGame::setupPawnTest() {
    
    /*
     
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |WP|  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |BP|  |BP|  |  |
     |  |  |  |  |WP|  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     
     */
    
    initPiece(PAWN_ENUM, WHITE, Position(6,6));
    initPiece(PAWN_ENUM, WHITE, Position(4, 3));
    initPiece(PAWN_ENUM, BLACK, Position(5, 4));
    initPiece(PAWN_ENUM, BLACK, Position(3, 4));
    
}

void TestChessGame::setupRookTest() {
    
    /*
     
     |  |  |  |  |BR|  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |BR|  |  |  |  |  |  |
     |WR|  |  |  |WR|  |  |  |
     
     */
    
    initPiece(ROOK_ENUM, WHITE, Position(0,0));
    initPiece(ROOK_ENUM, WHITE, Position(4, 0));
    initPiece(ROOK_ENUM, BLACK, Position(1, 1));
    initPiece(ROOK_ENUM, BLACK, Position(4, 7));
    
}

void TestChessGame::setupKnightTest() {
    
    /*
     
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |WK|  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |WK|  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |BK|  |  |  |  |
     
     */
    
    initPiece(KNIGHT_ENUM, WHITE, Position(4,2));
    initPiece(KNIGHT_ENUM, WHITE, Position(3,4));
    initPiece(KNIGHT_ENUM, BLACK, Position(3,0));
    
}

void TestChessGame::setupBishopTest() {

   /*
     
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |WB|  |  |  |BB|  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |WB|  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     
     */
    
    initPiece(BISHOP_ENUM, WHITE, Position(3, 2));
    initPiece(BISHOP_ENUM, WHITE, Position(1, 4));
    initPiece(BISHOP_ENUM, BLACK, Position(5,4));

}

void TestChessGame::setupQueenTest() {
    
    /*
     
     |WQ|  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |WQ|  |  |  |BQ|
     
     */
    
    initPiece(QUEEN_ENUM, WHITE, Position(0, 7));
    initPiece(QUEEN_ENUM, WHITE, Position(3, 0));
    initPiece(QUEEN_ENUM, BLACK, Position(7,0));
    
}

void TestChessGame::setupKingTest() {
    
    /*
     
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |WK|BK|  |  |  |
     |  |  |  |WK|  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     |  |  |  |  |  |  |  |  |
     
     */
    
    initPiece(KING_ENUM, WHITE, Position(3, 3));
    initPiece(KING_ENUM, WHITE, Position(3, 4));
    initPiece(KING_ENUM, BLACK, Position(4, 4));
    
}

bool testKingMoves() {
    
    TestChessGame board;
    board.setupKingTest();
    
    Position p3_3 = Position(3,3);
    Position p3_4 = Position(3,4);
    Position p4_4 = Position(4,4);
    
    Piece *piece3_3 = board.getPiece(p3_3);
    Piece *piece3_4 = board.getPiece(p3_4);
    Piece *piece4_4 = board.getPiece(p4_4);
    
    bool validMove1 = piece3_3->validMove(p3_3, p4_4, board) > 0;
    bool validMove2 = piece3_4->validMove(p3_4, p4_4, board) > 0;
    bool validMove3 = piece4_4->validMove(p4_4, p3_4, board) > 0;
    bool validMove4 = piece3_3->validMove(p3_3, Position(3, 2), board) > 0;   
    
    bool invalidMove1 = piece3_3->validMove(p3_3, p3_4, board) <= 0;
    bool invalidMove2 = piece3_3->validMove(p3_3, Position(0, 3), board) <= 0;
    bool invalidMove3 = piece4_4->validMove(p4_4, Position(6, 4), board) <= 0;

    return validMove1 && validMove2 && validMove3 && validMove4 && invalidMove1 && invalidMove2 && invalidMove3;
    
}

bool testQueenMoves() {
    
    TestChessGame board;
    board.setupQueenTest();
    
    Position p0_7 = Position(0,7);
    Position p3_0 = Position(3,0);
    Position p7_0 = Position(7,0);
    
    Piece *piece0_7 = board.getPiece(p0_7);
    Piece *piece3_0 = board.getPiece(p3_0);
    Piece *piece7_0 = board.getPiece(p7_0);
    
    bool validMove1 = piece0_7->validMove(p0_7, Position(7,7), board) > 0;
    bool validMove2 = piece0_7->validMove(p0_7, p7_0, board) > 0;
    bool validMove3 = piece3_0->validMove(p3_0, p7_0, board) > 0;
    
    bool invalidMove1 = piece0_7->validMove(p0_7, Position(1, 5), board) <= 0;
    bool invalidMove2 = piece7_0->validMove(p7_0, Position(6, 7), board) <= 0;
    
    return validMove1 && validMove2 && validMove3 && invalidMove1 && invalidMove2;
    
    
}

bool testBishopMoves() {
    
    TestChessGame board;
    board.setupBishopTest();
    
    Position p3_2 = Position(3,2);
    Position p1_4 = Position(1,4);
    Position p5_4 = Position(5,4);
    
    Piece *piece3_2 = board.getPiece(p3_2);
    Piece *piece1_4 = board.getPiece(p1_4);
    Piece *piece5_4 = board.getPiece(p5_4);
    
    bool validMove1 = piece3_2->validMove(p3_2, p5_4, board) > 0;
    bool validMove2 = piece3_2->validMove(p3_2, Position(5, 0), board) > 0;
    bool validMove3 = piece1_4->validMove(p1_4, Position(3, 6), board) > 0;
    bool validMove4 = piece5_4->validMove(p5_4, p3_2, board) > 0;
    
    bool invalidMove1 = piece3_2->validMove(p3_2, Position(3, 4), board) <= 0;
    bool invalidMove2 = piece3_2->validMove(p3_2, p1_4, board) <=0;
    bool invalidMove3 = piece5_4->validMove(p5_4, Position(5, 6), board) <= 0;
    
    return validMove1 && validMove2 && validMove3 && validMove4 && invalidMove1 && invalidMove2 && invalidMove3;

}

bool testKnightMoves() {
    
    TestChessGame board;
    board.setupKnightTest();
    
    Position p4_2 = Position(4,2);
    Position p3_4 = Position(3,4);
    Position p3_0 = Position(3,0);
    
    Piece *piece4_2 = board.getPiece(p4_2);
    Piece *piece3_0 = board.getPiece(p3_0);
    
    bool validMove1 = piece4_2->validMove(p4_2, p3_0, board) > 0;
    bool validMove2 = piece4_2->validMove(p4_2, Position(5,0), board) > 0;
    bool validMove3 = piece4_2->validMove(p4_2, Position(6, 1), board) > 0;
    bool validMove4 = piece3_0->validMove(p3_0, p4_2, board) > 0;
    
    bool invalidMove1 = piece4_2->validMove(p4_2, p3_4, board) <= 0;
    bool invalidMove2 = piece4_2->validMove(p4_2, Position(6, 2), board) <= 0;
    bool invalidMove3 = piece4_2->validMove(p4_2, Position(5,2), board) <= 0;
    
    return validMove1 && validMove2 && validMove3 && validMove4 && invalidMove1 && invalidMove2 && invalidMove3;
}



bool testRookMoves() {
    
    TestChessGame board;
    board.setupRookTest();
    
    Position p0_0 = Position(0,0);
    Position p4_0 = Position(4,0);
    Position p4_7 = Position(4,7);
    
    Piece *piece0_0 = board.getPiece(p0_0);
    Piece *piece4_0 = board.getPiece(p4_0);
    
    bool validMove1 = piece0_0->validMove(p0_0, Position(0, 5), board) > 0;
    bool validMove2 = piece0_0->validMove(p0_0, Position(1, 0), board) > 0;
    bool validMove3 = piece4_0->validMove(p4_0, p4_7, board) > 0;
    
    bool invalidMove1 = piece0_0->validMove(p0_0, Position(1, 1), board) <= 0;
    bool invalidMove2 = piece0_0->validMove(p0_0, p4_0, board) <= 0;
    
    return validMove1 && validMove2 && validMove3 && invalidMove1 && invalidMove2;
    
}

bool testPawnMoves() {
    
    TestChessGame board;
    board.setupPawnTest();
    
    Position p6_6 = Position(6,6);
    Position p4_3 = Position(4,3);
    Position p5_4 = Position(5,4);
    Position p3_4 = Position(3,4);
    
    Piece *piece6_6 = board.getPiece(p6_6);
    Piece *piece4_3 = board.getPiece(p4_3);
    Piece *piece5_4 = board.getPiece(p5_4);
    Piece *piece3_4 = board.getPiece(p3_4);
    
    bool pawnToQueen1 = piece6_6->validMove(p6_6, Position(6,7), board) == MOVE_PAWN2QUEEN;
    
    bool validMove1 = piece4_3->validMove(p4_3, p5_4, board) == SUCCESS;
    bool validMove2 = piece4_3->validMove(p4_3, p3_4, board) == SUCCESS;
    bool validMove3 = piece4_3->validMove(p4_3, Position(4,4), board) == SUCCESS;
    bool validMove4 = piece3_4->validMove(p3_4, p4_3, board) == SUCCESS;
    bool validMove5 = piece5_4->validMove(p5_4, p4_3, board) == SUCCESS;
    
    bool invalidMove1 = piece4_3->validMove(p4_3, Position(4,2), board) == MOVE_ERROR_ILLEGAL;
    bool invalidMove2 = piece4_3->validMove(p4_3, Position(3,3), board) == MOVE_ERROR_ILLEGAL;
    bool invalidMove3 = piece4_3->validMove(p4_3, Position(5,3), board) == MOVE_ERROR_ILLEGAL;
    
    return pawnToQueen1 && validMove1 && validMove2 && validMove3 && validMove4 && validMove5 && invalidMove1 && invalidMove2 && invalidMove3;
    
}

int main() {
    
    bool passedAll = true;

    if (testPawnMoves()) {
        std::cout << "Pawn Moves Passed\n";
    }
    else {
        std::cout << "Pawn Moves Failed\n";
        passedAll = false;
    }
    
    if (testRookMoves()) {
        std::cout << "Rook Moves Passed\n";
    }
    else {
        std::cout << "Rook Moves Failed\n";
        passedAll = false;
    }

    if (testKnightMoves()) {
        std::cout << "Knight Moves Passed\n";
    }
    else {
        std::cout << "Knight Moves Failed\n";
        passedAll = false;
    }
    
    if (testBishopMoves()) {
        std::cout << "Bishop Moves Passed\n";
    }
    else {
        std::cout << "Bishop Moves Failed\n";
        passedAll = false;
    }
    
    if (testQueenMoves()) {
        std::cout << "Queen Moves Passed\n";
    }
    else {
        std::cout << "Queen Moves Failed\n";
        passedAll = false;
    }
    
    if (testKingMoves()) {
        std::cout << "King Moves Passed\n";
    }
    else {
        std::cout << "King Moves Failed\n";
        passedAll = false;
    }
    
    if (passedAll) {
        std::cout << "All Tests Passed\n";
    }
    else {
        std::cout << "Some Tests Failed\n";
    }

}
