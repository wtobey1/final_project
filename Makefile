CXX = g++
CXXFLAGS = -Wall -Wextra -pedantic -std=c++11 -g

main: main.o Board.o Chess.o
	$(CXX) $(CXXFLAGS) -o main main.o Chess.o Board.o

test: unittest.o Board.o Chess.o
	$(CXX) $(CXXFLAGS) -o test unittest.o Chess.o Board.o

unittest.o: unittest.cpp Chess.h
	$(CXX) $(CXXFLAGS) -c unittest.cpp

chess: Chess.o Board.o
	$(CXX) $(CXXFLAGS) Chess.o Board.o -o chess

Board.o: Game.h Board.cpp
	$(CXX) $(CXXFLAGS) -c Board.cpp

Chess.o: Game.h Chess.h Prompts.h Terminal.h
	$(CXX) $(CXXFLAGS) -c Chess.cpp

test_end2end:
	./test.sh 4

clean:
	rm *.o main
	
