#include "Chess.h"
#include "Prompts.h"
#include <cstdio>
#include <stdlib.h>
#include "Terminal.h"

int main() {

  ChessGame game;
  game.setupBoard();
  game.display();

}
