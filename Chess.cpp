#undef FOR_RELEASE

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <string>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include "Terminal.h"

//Loops turns for chess game
void ChessGame::run(){

    //Create prompts
    Prompts p;

    //Set a toggle int value for board
    int board = 0;

    //Set two intial players
    Player p1 = playerTurn();
    Player p2 = playerTurn();
    if(p1 == 0){
        p2 = BLACK;
    }else{
        p2 = WHITE;
    }

    //Runs the game until it is over
    int gameOver = 0;
    while(gameOver == 0){

        //Create int for while loop and input strings
        int ex = 0;
        std::string s;
        std::string e;

        //Ask user for input until correct one given
        while(ex == 0){
            
            //At the start, check if board is toggled, prompt player
            if(board == 1){ display(); }

            //Prompts the player for input
            p.playerPrompt(p1, turn());

            //Take in the first string, case insensitive
            std::cin >> s;
            for(unsigned int str = 0; str < s.length(); str++){
                s.at(str) = tolower(s.at(str));
            }

            //Checks if the input is correct and how to react
            if(s == std::string("q")){
            
                gameOver = 1;
                break;

            }else if(s == std::string("save")){

                // Do we need to chekc for the file?
                
                p.saveGame();
                std::string name;
                std::cin >> name;

                std::ofstream file;
                file.open (name);

                bool result = saveBoard(file);

                file.close();

                if(result == false){
                    p.saveFailure();
                }

            }else if(s == std::string("forfeit")){
               
                p.win(p2, turn());
                p.gameOver();
                gameOver = 1;
                std::cout << '\n';
                break;
            
            }else if(s == std::string("board")){
            
                if(board == 1){ board = 0; }
                else{ board = 1; }

            }else{

                //Checks if the first string was valid location
                bool isStartInvalid = ((s.size() != 2) || tolower(s.at(0)) < 'a' || tolower(s.at(0)) > 'z') || (s.at(1) < '0' || s.at(1) > '9');

                //If invalid print Error and run another iteration of the loop
                if(isStartInvalid){
                    p.parseError();
                    continue;
                }


                //Checks for the end location
                std::cin >> e;

                //Checks if the end location is valid
                bool isEndInvalid = ((e.size() != 2) || tolower(e.at(0)) < 'a' || tolower(e.at(0)) > 'z') || (e.at(0) < '0' || e.at(1) > '9');

                //If the moves are invalid, print error and ask again
                if(isEndInvalid){
                    p.parseError();
                }else{
                    ex = 1;
                }
            }

        }

        //Creates the proper positions if a move input was given
        if(ex == 1){

            int startX =  tolower(s.at(0)) - 'a';
            int startY = s.at(1) - '0' - 1;
            int endX = tolower(e.at(0)) - 'a';
            int endY = e.at(1) - '0' - 1;

            Position start(startX, startY);
            Position end(endX, endY);

            int move = makeMove(start, end);
            
            //Switch to p2 *AND MAKE IT AUTO END
            if(isCheck(p2)){
                if(noValidMove(p2)){ move = MOVE_CHECKMATE; }
                else{ move = MOVE_CHECK; }
            }
            else{
                if(noValidMove(p2)){ move = MOVE_STALEMATE; } 
            }
           
            switch(move){
                case MOVE_ERROR_OUT_OF_BOUNDS: p.outOfBounds(); break;
                case MOVE_ERROR_NO_PIECE: p.noPiece(); break;
                case MOVE_ERROR_BLOCKED: p.blocked(); break;
                case MOVE_ERROR_CANT_CASTLE: p.cantCastle(); break;
                case MOVE_ERROR_MUST_HANDLE_CHECK: p.mustHandleCheck(); break;
                case MOVE_ERROR_CANT_EXPOSE_CHECK: p.cantExposeCheck(); break;
                case MOVE_ERROR_ILLEGAL: p.illegalMove(); break;
                case SUCCESS: break;
                case MOVE_CHECK: p.check(p1); break;
                case MOVE_CAPTURE: p.capture(p1); break;
                case MOVE_CASTLE: std::cout << "Castle\n"; break;
                case MOVE_CHECKMATE: p.checkMate(p1); p.win(p1, turn()); p.gameOver(); gameOver = 1; break;
                case MOVE_STALEMATE: p.staleMate(); p.gameOver(); gameOver = 1; break;
                case GAME_WIN: p.win(p1, turn()); p.gameOver(); gameOver = 1; break;
                case GAME_OVER: p.gameOver(); gameOver = 1; break;

            }


            //if the move doesn't have an error it goes to next turn
            if(move > 0){
                m_turn++;
                Player temp = p1;
                p1 = p2;
                p2 = temp;
            } 
        }
    }

}

// Not responsible for opening and closing file
bool ChessGame::saveBoard(std::ofstream &file) const {

    // Write game type
    file << "chess \n";

    // Says last turn taken on Piazza, not sure if this is what they want
    file << m_turn - 1 << '\n';

    int count = 0;

    // Loop through pieces
    for ( Piece *p : m_pieces ) {

        // Get position from count
        Position position = positionFromIndex(count++);

        // Skip nulls
        if (p == nullptr) {
            continue;
        }

        // Print piece information
        file << p->owner() << " ";
        file << (char) ('a' + position.x) << position.y + 1;
        file << " " << p->id();
        file << '\n';

    }

    return true;

}

// Make a move on the board. Return an int, with < 0 being failure
int ChessGame::makeMove(Position start, Position end) {

    //Gets the piece at the start index
    Piece* P = getPiece(start);   
  
    //If there is no piece in position start
    if(P == nullptr){
        if(validPosition(start)){
            return MOVE_ERROR_NO_PIECE;
        }else{
            return MOVE_ERROR_OUT_OF_BOUNDS;
        }
    }

    // Makes sure that the owner is the current player
    if (P->owner() != playerTurn()) {
        return MOVE_ERROR_NO_PIECE;
    }

    //See if the current player is in check at start of turn
    bool inCheck = isCheck(playerTurn());

    //If valid piece, check if there is a valid move
    int a = P->validMove(start, end, *this);

    //Given that its a valid move, move the piece unless you are still in check after the movement
    if(a > 0) {
        
        // Make sure that the move does not cause the player to expose their king
        if (causesCheck(start, end,  playerTurn())) {
            return inCheck ? MOVE_ERROR_MUST_HANDLE_CHECK : MOVE_ERROR_CANT_EXPOSE_CHECK;
        }

        // Check if a pawn reaches the end of the board
        if (a == MOVE_PAWN2QUEEN) {

            // Remove the pawn and initiate a queen in its place
            m_pieces[index(start)] = nullptr;
            initPiece(QUEEN_ENUM, P->owner(), start);

            // Free the old pawn
            delete P;

            // Retrieve the new Queen
            P = getPiece(start);

        }

        // Needed for castling (if first move, add it to moved pieces)
        if (std::find_if(movedPieces.begin(), movedPieces.end(),
                    [start](const Position &f) -> bool {

                    return f.x == start.x && f.y ==start.y;

                    }) == movedPieces.end()) {

                movedPieces.push_back(start);

                }


        // User has captured a piece if it exists at the position
        if (getPiece(end) != nullptr) {
            a = MOVE_CAPTURE;
            delete getPiece(end);
        }

        // If a castle move is possible, check if player is currently checked (which you can't castle out of)
        if(a == MOVE_CASTLE){
            if(inCheck){ a = MOVE_ERROR_CANT_CASTLE; }
            else{
                int diffx = end.x - start.x;
                int x = diffx > 0 ? 7 : 0;
                int endX = diffx > 0 ? end.x - 1 : end.x + 1;

                Position rStart(x, end.y);
                Position rEnd(endX, end.y);

                Piece* r = getPiece(rStart);

                m_pieces[index(rEnd)] = r;
                m_pieces[index(rStart)] = nullptr;
            }
        }


        // Performing the inputed move by user
        m_pieces[index(end)] = P;
        m_pieces[index(start)] = nullptr;
        
    }

    return a;


    // Feel free to use this or change it as you see fit
//    int retCode = Board::makeMove(start, end);
//    return retCode;
}

// Setup the chess board with its initial pieces
void ChessGame::setupBoard() {
    std::vector<int> pieces {
        ROOK_ENUM, KNIGHT_ENUM, BISHOP_ENUM, QUEEN_ENUM,
        KING_ENUM, BISHOP_ENUM, KNIGHT_ENUM, ROOK_ENUM
    };
    for (size_t i = 0; i < pieces.size(); ++i) {
        initPiece(PAWN_ENUM, WHITE, Position(i, 1));
        initPiece(pieces[i], WHITE, Position(i, 0));
        initPiece(pieces[i], BLACK, Position(i, 7));
        initPiece(PAWN_ENUM, BLACK, Position(i, 6));
    }
}

void ChessGame::setupBoard(std::ifstream &file) {

    std::string game;
    file >> game;

    file >> m_turn;
    m_turn += 1;

    int player = 0;
    std::string string;
    int pieceType = 0;

    while (file >> player && file >> string && file >> pieceType) {
        int x = (int) (string.at(0) - 'a');
        int y = string.at(1) - '0' - 1;
        
        initPiece(pieceType, Player(player), Position(x, y));
    }  

}

// Retrieves the expected color for a cell on the board
void setColorForCell(int h, int w) {
    
    if (!(h % 2) && (w % 2)) { Terminal::colorFg(false, Terminal::BLACK); Terminal::colorLightBoard(); }
    else if ((h % 2) && (w % 2)) { Terminal::colorFg(false, Terminal::WHITE); Terminal::colorDarkBoard(); }
    else if (!(h % 2) && !(w % 2)) { Terminal::colorFg(false, Terminal::WHITE); Terminal::colorDarkBoard(); }
    else if ((h % 2) && !(w % 2)) { Terminal::colorFg(false, Terminal::BLACK); Terminal::colorLightBoard();  }

}

// Creates a blank row matching the cell color
void printBlankCellRow(int h, int w) {

    setColorForCell(h, w);
    std::cout << "     ";

}

// Prints an empty set of spaces where the row number goes
void printBlankLabelRow() {
    
    Terminal::colorFg(true, Terminal::WHITE); 
    Terminal::colorBg(Terminal::BLACK); 

    // Prints background three spaces
    std::cout << "   ";

    // Prints empty cell in between board
    Terminal::set_default();
    std::cout << " ";

}

// For printing the board
void ChessGame::display() {

    std::map<int, std::string> ids = { {0, " P "}, {1, " R "}, {2, "K N"}, {3, " B "}, {4, " Q "}, {5, " K "} };
    
    Terminal::set_default();
    std::cout << '\n';
    // Counts down since rows start at 8 go to 1
    for (unsigned int h = height(); h > 0; h--) {

        // Prints a blank row before the chess board
        printBlankLabelRow();
        
        // Creates top row of cells in the board
        for (unsigned int w = 1; w <= width(); w++) {
            printBlankCellRow(h, w);
        }
        
        // Reset colors
        Terminal::set_default();     

        // Write the row number
        std::cout << '\n' << " " << h << " ";
        
        // Space between the board
        std::cout << " ";

        // Creates data row
        for (unsigned int w = 1; w <= width(); w++) {

            // Set the color for the cell
            setColorForCell(h, w);

            // Get the piece at that cell if one exists
            Piece *piece = getPiece(Position(w-1, h-1));

            // Print spaces if null cell
            if (piece == nullptr) {
                std::cout << "     ";
            }
            else {
                // Print appropriate color if cell is
                if (piece->owner() == BLACK) { Terminal::colorFg(false, Terminal::BLACK); }
                else { Terminal::colorFg(false, Terminal::WHITE); }
                std::cout << " " << ids[piece->id()] << " ";
            }

        }

        // Return
        Terminal::set_default();
        std::cout << '\n';

        // Prints the last row of the label
        printBlankLabelRow();

        // Creates bottom cells in that row
        for (unsigned int w = 1; w <= width(); w++) {
            printBlankCellRow(h, w);
        }

        Terminal::set_default();
        // Returns
        std::cout << '\n';

        // Resets colors
        Terminal::set_default();

    }

    // Spaces between board and column names
    std::cout << '\n';

    Terminal::set_default();

    // Print empty space holder for spacing with the row headers
    printBlankLabelRow();

    // Reset colors
    Terminal::set_default();

    // Writes the column names
    for (unsigned int w = 1; w <= width(); w++) {

        std::cout << "  " << (char)('A' + w - 1) << "  ";
        
    }

    std::cout << "\n\n";

    // Resets colors
    Terminal::set_default();


}

 bool ChessGame::noValidMove(Player p) {
    
        int count = 0;
        
        // Goes throguh all pieces
        for ( Piece *piece : m_pieces ){
  
            // Goes through all posible end locations
            for(int x = 0; x < 64; x++){
                
                // Checks for a valid move of any piece of color p
                if (piece != nullptr && piece->owner() == p && piece->validMove(positionFromIndex(count), positionFromIndex(x), *this) > 0){    

                    //If valid move, see if ends in check
                    if(!causesCheck(positionFromIndex(count), positionFromIndex(x), p)){
                        std::cout << count << ' ' << x << '\n';
                        return false;
                    }
                }
            }
            count++;

        }

        return true;

}

bool ChessGame::isCheck(Player p) {

        // Gets the index of the current players king
        int king = getKing(p);

        // If for some reason there is not king returns
        if (king == -1) { return false; };

        int count = 0;

        // Goes throguh all pieces
        for ( Piece *piece : m_pieces){
            
            // Checks if moving to king is a valid move
            if (piece != nullptr && piece->validMove(positionFromIndex(count), positionFromIndex(king), *this) > 0) {
                // If so then you are in check
                return true;
            }

            count++;

        }

        return false;

    }

// PIECE VALID MOVES

int Pawn::validMove(Position start, Position end,
        const Board& board) const
{

    int generic = Piece::validMove(start, end, board);

    if (generic != (int) SUCCESS) {
        return generic;
    }

    unsigned int finalRow = (owner() == WHITE ? 7 : 0);

    std::vector<Position> p;

    if (start.y == (owner() == WHITE ? 2 - 1 : 7 - 1)) {
        p.push_back(Position(start.x, start.y + (owner() == Player::WHITE ? 2 : -2)));
    }

    p.push_back(Position(start.x, start.y + (owner() == Player::WHITE ? 1 : -1)));
    p.push_back(Position(start.x + 1, start.y + (owner() == Player::WHITE ? 1 : -1)));
    p.push_back(Position(start.x - 1, start.y + (owner() == Player::WHITE ? 1 : -1)));

    // Check if the move is possible for a pawn
    if (std::find_if(p.begin(), p.end(), 
                [end](const Position &f) -> bool {

                return f.x == end.x && f.y == end.y;
                
                }) ==  p.end()) {

        return MOVE_ERROR_ILLEGAL;
    }

    // Any forward moves can be checked for blockage
    Piece* piece = board.getPiece(end);   

    // Checks if it is a forward move
    if (end.x == start.x) {

        // Checks if move is blocked
        if (piece == nullptr) {
            return end.y == finalRow ? MOVE_PAWN2QUEEN : SUCCESS;
        }
        else {
            // Piece is already in that position
            return MOVE_ERROR_BLOCKED;
        }
    }

    else {

        // If no piece at diagonal, illegal move
        if (piece== nullptr) {
            return MOVE_ERROR_ILLEGAL;
        }
        // If a piece at diagonal is players piece, illegal
        else if (piece->owner() == owner()) {
            return MOVE_ERROR_ILLEGAL;
        }
        // If piece at diagonal is opponents piece legal
        else {
            return end.y == finalRow ? MOVE_PAWN2QUEEN : SUCCESS;

        }
    }

    return SUCCESS;
}

 int Rook::validMove(Position start, Position end,
        const Board& board) const {

    // Calls genewric method
    int generic = this->Piece::validMove(start, end, board);
    if (generic != SUCCESS) {
        return generic;
    }

    // Makes sure user moves in only 1 direction
    if (start.y != end.y && start.x != end.x) {
        return MOVE_ERROR_ILLEGAL;
    }

    unsigned int x;
    unsigned int y;

    // Sets the increment to 0 if they are the same, 1 if end is greater than start, and -1 if start is greater than end
    int xIncrement = start.x == end.x ? 0 : (start.x < end.x ? 1 : -1);
    int yIncrement = start.y == end.y ? 0 : (start.y < end.y ? 1 : -1);

    // Checks all pieces in diagonal
    for (x = start.x + xIncrement, y = start.y + yIncrement; !(x == end.x && y == end.y); x += xIncrement, y += yIncrement) {
        if (board.getPiece(Position(x, y)) != nullptr) { return MOVE_ERROR_BLOCKED; }

    }

    Piece* endPiece = board.getPiece(end);

    if (endPiece == nullptr) { return SUCCESS; }
    else {
        if (endPiece->owner() == owner()) { return MOVE_ERROR_BLOCKED; }
        else { return MOVE_CAPTURE; }
    }

    
    // Check if the piece is directly forward or sideways in a direction
    // Checks if all cells in that direction are empty
    return SUCCESS; 
}
int Knight::validMove(Position start, Position end,
        const Board& board) const  { 

        int generic = this->Piece::validMove(start, end, board);
        if (generic != SUCCESS) {
            return generic;
        }

        int xDiff = std::max(start.x, end.x) - std::min(start.x, end.x);
        int yDiff = std::max(start.y, end.y) - std::min(start.y, end.y);

        float ratio = xDiff / (float) yDiff;

        // Checks to make sure the move is 2x1 or 1x2
        if (ratio != 0.5 && ratio != 2.0) {
            return MOVE_ERROR_ILLEGAL;
        }

        // If neither of them are 1, it must be illegal
        if (xDiff != 1 && yDiff != 1) {
            return MOVE_ERROR_ILLEGAL;
        }

        Piece* piece = board.getPiece(end);

        if (piece != nullptr) {

            if (owner() == piece->owner()) { return MOVE_ERROR_BLOCKED; }
            else { return MOVE_CAPTURE; }

        }
        else {
            return SUCCESS;
        }
       
        return SUCCESS; 
    
    }
int Bishop::validMove(Position start, Position end,
        const Board& board) const { 

    int generic = this->Piece::validMove(start, end, board);
    if (generic != SUCCESS) {
        return generic;
    }

    int xDiff = std::max(start.x, end.x) - std::min(start.x, end.x);
    int yDiff = std::max(start.y, end.y) - std::min(start.y, end.y);
    
    // Checks to make sure ratio is 1 to 1
    if (xDiff != yDiff) {
        return MOVE_ERROR_ILLEGAL;
    }
    
    unsigned int x;
    unsigned int y;

    int xIncrement = start.x < end.x ? 1 : -1;
    int yIncrement = start.y < end.y ? 1 : -1;


    // Checks all pieces in diagonal
    for (x = start.x + xIncrement, y = start.y + yIncrement; !(x == end.x && y == end.y); x += xIncrement, y += yIncrement) {

        
        if (board.getPiece(Position(x, y)) != nullptr) { return MOVE_ERROR_BLOCKED; }

    }

    Piece* endPiece = board.getPiece(end);

    // Checks end piece
    if (endPiece == nullptr) { return SUCCESS; }
    else {
        if (endPiece->owner() == owner()) { return MOVE_ERROR_BLOCKED; }
        else { return MOVE_CAPTURE; }
    }
    
    return SUCCESS; 
}

int Queen::validMove(Position start, Position end,
        const Board& board) const { 

    int generic = this->Piece::validMove(start, end, board);
    if (generic != SUCCESS) {
        return generic;
    }

    int xDiff = std::max(start.x, end.x) - std::min(start.x, end.x);
    int yDiff = std::max(start.y, end.y) - std::min(start.y, end.y);

    // Checks to make sure ratio is 1 to 1 or either x or y is held constant
    if (xDiff != yDiff && (xDiff != 0 && yDiff != 0)) {
        return MOVE_ERROR_ILLEGAL;
    }

    unsigned int x;
    unsigned int y;

    // Sets the increment to 0 if they are the same, 1 if end is greater than start, and -1 if start is greater than end
    int xIncrement = start.x == end.x ? 0 : (start.x < end.x ? 1 : -1);
    int yIncrement = start.y == end.y ? 0 : (start.y < end.y ? 1 : -1);;

    // Checks all pieces in diagonal
    for (x = start.x + xIncrement, y = start.y + yIncrement; !(x == end.x && y == end.y); x += xIncrement, y += yIncrement) {
        
        if (board.getPiece(Position(x, y)) != nullptr) { return MOVE_ERROR_BLOCKED; }

    }

    Piece* endPiece = board.getPiece(end);

    if (endPiece == nullptr) { return SUCCESS; }
    else {
        if (endPiece->owner() == owner()) { return MOVE_ERROR_BLOCKED; }
        else { return MOVE_CAPTURE; }
    }
    
    return SUCCESS; 

}


 int King::validMove(Position start, Position end,
        const Board& board) const {
        
    int generic = this->Piece::validMove(start, end, board);
    if (generic != SUCCESS) {
        return generic;
    }

    int xDiff = std::max(end.x, start.x) - std::min(end.x, start.x);
    int yDiff = std::max(end.y, start.y) - std::min(end.y, start.y);

    Position kingPosition = Position(4, owner() == WHITE ? 0 : 7);

    // Checks if a castling move is being made
    if (xDiff == 2 && yDiff == 0 && (start.x == kingPosition.x && start.y == kingPosition.y)) {

        int signedXDiff = start.x - end.x;

        // Gets the expected position of the rook that the king is moving towards
        Position rookPosition = Position(signedXDiff > 0 ? 0 : 7, owner() == WHITE ? 0 : 7);
        
        // Checks if rook has been moved
        bool rookMoved = std::find_if(board.movedPieces.begin(), board.movedPieces.end(), 
                [rookPosition](const Position &f) -> bool {

                return f.x == rookPosition.x && f.y == rookPosition.y;

                }) != board.movedPieces.end();
        
        // Checks if king (current piece) has been moved
        bool kingMoved = std::find_if(board.movedPieces.begin(), board.movedPieces.end(), 
                [kingPosition](const Position &f) -> bool {
                
                return f.x == kingPosition.x && f.y == kingPosition.y;

                }) != board.movedPieces.end();
     
        // If neither have been moved execute this. If one has been moved, then the move will be caught as MOVE_ERROR_ILLEGAL in the next if
        if (!rookMoved && !kingMoved) { 


            // Increments towards the rook
            int xIncrement = 1 * signedXDiff > 0 ? -1 : 1;

            // Checks if any pieces are between the rook and the king
            for (unsigned int x = start.x + xIncrement; x != end.x; x += xIncrement) {
                if (board.getPiece(Position(x, end.y)) != nullptr) { return MOVE_ERROR_CANT_CASTLE; }
            }

            // If no pieces are in between, castling is valid
            return MOVE_CASTLE;
        }
    }

    if (!(xDiff <= 1 && yDiff <= 1)) { return MOVE_ERROR_ILLEGAL; }

    
    Piece* endPiece = board.getPiece(end);

    // Checks end piece
    if (endPiece == nullptr) { return SUCCESS; }
    else {
        if (endPiece->owner() == owner()) { return MOVE_ERROR_BLOCKED; }
        else { return MOVE_CAPTURE; }
    }
    
    return SUCCESS; 

}

